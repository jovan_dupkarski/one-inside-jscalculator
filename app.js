class Calculator {
    constructor(previousElement, currentElement) {
      this.previousElement = previousElement
      this.currentElement = currentElement
      this.clear()
    };
  
    clear() {
      this.currentOperand = ''
      this.previousOperand = ''
      this.operation = undefined
    };
  
    delete() {
      this.currentOperand = this.currentOperand.toString().slice(0, -1)
    };
  
    addNumber(number) {
      if (number === '.' && this.currentOperand.includes('.')) return
      this.currentOperand = this.currentOperand.toString() + number.toString()
    };
  
    chooseOperation(operation) {
      if (this.currentOperand === '') return
      if (this.previousOperand !== '') {
        this.compute()
      }
      this.operation = operation
      this.previousOperand = this.currentOperand
      this.currentOperand = ''
    };
  
    compute() {
      let computation
      const prev = parseFloat(this.previousOperand)
      const current = parseFloat(this.currentOperand)
      if (isNaN(prev) || isNaN(current)) return
      switch (this.operation) {
        case '+':
          computation = prev + current
          break
        case '-':
          computation = prev - current
          break
        case '*':
          computation = prev * current
          break
        case '÷':
          computation = prev / current
          break
        default:
          return
      }
      this.currentOperand = computation
      this.operation = undefined
      this.previousOperand = ''
    };
  
    getDisplayNumber(number) {
      const stringNumber = number.toString()
      const integerDigits = parseFloat(stringNumber.split('.')[0])
      const decimalDigits = stringNumber.split('.')[1]
      let integerDisplay
      if (isNaN(integerDigits)) {
        integerDisplay = ''
      } else {
        integerDisplay = integerDigits.toLocaleString('en', { maximumFractionDigits: 0 })
      }
      if (decimalDigits != null) {
        return `${integerDisplay}.${decimalDigits}`
      } else {
        return integerDisplay
      };
    };
  
    updateDisplay() {
      this.currentElement.innerText =
        this.getDisplayNumber(this.currentOperand)
      if (this.operation != null) {
        this.previousElement.innerText =
          `${this.getDisplayNumber(this.previousOperand)} ${this.operation}`
      } else {
        this.previousElement.innerText = ''
      }
    }
  };
  

  
  
  const numberBtn = document.querySelectorAll('[number]');
  const operationBtn = document.querySelectorAll('[operation]');
  const equalBtn = document.querySelector('[equal]');
  const deleteBtn = document.querySelector('[delete]');
  const clearBtn = document.querySelector('[clear]');
  const previousElement = document.querySelector('[previous]');
  const currentElement = document.querySelector('[current]');
  
  const calculator = new Calculator(previousElement, currentElement);







  numberBtn.forEach(button => {
    button.addEventListener('click', debounce(() => {
      calculator.appendNumber(button.innerText)
      calculator.updateDisplay()
    }, 300))
  });
  
  numberBtn.forEach(button => {
    button.addEventListener('click', () => {
      calculator.addNumber(button.innerText)
      calculator.updateDisplay()
    })
  });
  operationBtn.forEach(button => {
    button.addEventListener('click', () => {
      calculator.chooseOperation(button.innerText)
      calculator.updateDisplay()
    })
  });
  
  equalBtn.addEventListener('click', button => {
    calculator.compute()
    calculator.updateDisplay()
  });
  
  clearBtn.addEventListener('click', button => {
    calculator.clear()
    calculator.updateDisplay()
  });
  deleteBtn.addEventListener('click', button => {
    calculator.delete()
    calculator.updateDisplay()
  });



  
// Same button is clicked 2 or more times in interval shorter than 300ms
const debounce = (func, delay) => {
  let debounceTimer
  return function() {
      const context = this
      const args = arguments
          clearTimeout(debounceTimer)
              debounceTimer
          = setTimeout(() => func.apply(context, args), delay)
  };
};


//Keyboard logika
  document.onkeydown = function(event) {
    let operators = ['+', '-', 'x', '÷', '^'];

    let key_press = String.fromCharCode(event.keyCode);
    let key_code = event.keyCode;
    let input = document.querySelector('#screen');
    let inputVal = input.innerHTML;
    let btnVal = this.innerHTML
    let lastChar = inputVal[inputVal.length - 1];
    let equation = inputVal;
  
    equation = equation.replace(/x/g, '').replace(/÷/g, '/').replace(/\^/g, '*');
  
   
    
      if(key_press==1) {
        input.innerHTML += key_press;
    }
      if(key_press==2) {
        input.innerHTML += key_press; 
    }
      if(key_press==3 || key_code == 32) {
        input.innerHTML += key_press; 
    }
      if(key_press==4) {
        input.innerHTML += key_press; 
    }
      if(key_press==5) {
        input.innerHTML += key_press; 
    }
      if(key_press==6 && event.shiftKey == false) {
        input.innerHTML += key_press; 
    }
      if(key_press==7) {
        input.innerHTML += key_press; 
    }
      if(key_press==8 && event.shiftKey == false) {
        input.innerHTML += key_press; 
    }
      if(key_press==9) {
        input.innerHTML += key_press; 
    }
      if(key_press==0) {
        input.innerHTML += key_press;
    }
    
    
    
      if ((inputVal != '' && operators.indexOf(lastChar) == -1 && key_code == 187 && event.shiftKey) || (key_code == 107) || (key_code == 61 && event.shiftKey)) {
        document.querySelector('#screen').innerHTML += '+';
    }
      if ((inputVal != '' && operators.indexOf(lastChar) == -1 && key_code == 189 && event.shiftKey) || (inputVal != '' && operators.indexOf(lastChar) == -1 && key_code == 107)) {
        document.querySelector('#screen').innerHTML += '-';
    }
      if ((inputVal != '' && operators.indexOf(lastChar) == -1 && key_code == 56 && event.shiftKey) || (inputVal != '' && operators.indexOf(lastChar) == -1 && key_code == 106)) {
        document.querySelector('#screen').innerHTML += 'x';
    }
      if ((inputVal != '' && operators.indexOf(lastChar) == -1 && key_code == 191) || (inputVal != '' && operators.indexOf(lastChar) == -1 && key_code == 111)) {
        document.querySelector('#screen').innerHTML += '÷';
    }

      if(key_code==13 || key_code==187 && event.shiftKey == false) {
        input.innerHTML = calculator.compute(equation);
        calculator.updateDisplay();
        //reset decimal added flag
        decimalAdded =false;
    }
      if(key_code==8 || key_code==46) {
        input.innerHTML = '';
        decimalAdded = false;
    }
  };
//